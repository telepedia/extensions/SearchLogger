# SearchLogger
SearchLogger is an extension that allows a wiki to track searches over time. It shows searches and how popular they are at `Special:SearchLog`. By default, only administrators can access this page.

This extension has dependencies on TelepediaMagic to generate the pagination for the SpecialPage. The extension was originally created by the Hydra Wiki team (Gamepedia), but has since been forked and is now maintained by Telepedia for modern MediaWiki.

# Installation
- Add `wfLoadExtension( 'SearchLogger' );` to your LocalSettings file.
- Run `update.php` to create the necessary tables. 
